#!perl
use 5.012;
use warnings FATAL => 'all';

use Test::More;

unless (eval {
    require Test::Pod::Coverage;
}) {
    plan(skip_all => 'Test::Pod::Coverage is not installed.');
}
Test::Pod::Coverage->import;

all_pod_coverage_ok();
